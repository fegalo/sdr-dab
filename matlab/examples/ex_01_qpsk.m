%QPSK banda base y pasobanda

close all;
clear all;
clc;

qpsk=[1+i 1-i i+1 -i-1];

fs=1000;   %frecuencia de muestreo de 1 KHz
R1=5;      %5 simbolos/segundo
T=fs/R1;    %muestras/simbolo

x=qpsk(randi(length(qpsk),10,1));

%% Con pulso rectamgular
coef_filt=ones(1,T);
xup=upfirdn(x,coef_filt,T,1);
figure(1);subplot(211);
plot(real(xup));ylabel('Re');
hold on;
subplot(212);
plot(imag(xup));ylabel('Im');
hold on;
%Instante de muestreo
subplot(211);
index=1:T:length(xup-1);
xdown=xup(index);% downsampling 
plot(index,real(xdown),'*','MarkerSize',8);
subplot(212);
plot(index,imag(xdown),'*','MarkerSize',8);

%% Con coseno
pulse = rcosine(1,T,'normal',0.35);
xupc=upfirdn(x,pulse,T,1);
figure(2);subplot(211);
plot(real(xupc));ylabel('Re');
hold on
subplot(212);
plot(imag(xupc));ylabel('Im');
hold on
%Instante de muestreo
subplot(211);
delay=3*T;%delay=dalay_symbols*T;
index=(1:T:(length(xupc)-2*delay))+delay;
xdownc=xupc(index);% downsampling 
plot(index,real(xdownc),'*','MarkerSize',8);
subplot(212);
plot(index,imag(xdownc),'*','MarkerSize',8);

% figure
% plot(abs(fft(xup)))
% figure
% plot(abs(fft(xupc)))