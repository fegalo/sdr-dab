% Showing OFDM spectrum

close all;
clear all;
clc;

qpsk=[1 -1 i -i];

fs=100;   %frecuencia de muestreo de 1 KHz
R=5;      %5 simbolos/segundo
T=fs/R;    %muestras/simbolo - ¿periodo simbolo?
N=10;   % Numero de simbolos a transmitir en cada subportadora

xk1=qpsk(randi(length(qpsk),N,1));
xk2=qpsk(randi(length(qpsk),N,1));
xk3=qpsk(randi(length(qpsk),N,1));

t=[0:T];
s1=0;
for n=1:N
    s1=[s1 xk1(n)*exp(j*2*pi*1*t/T)];
end
s1=s1(2:end);
figure(1);
plot(imag(s1))


s2=0;
for n=1:N
    s2=[s2 xk2(n)*exp(j*2*pi*2*t/T)];
end
s2=s2(2:end);
figure(2);
plot(imag(s2))

s3=0;
for n=1:N
    s3=[s3 xk3(n)*exp(j*2*pi*3*t/T)];
end
s3=s3(2:end);
figure(3);
plot(imag(s3))

figure(4)
hold on
plot(abs(fft(s1)),'r')
plot(abs(fft(s2)),'g')
plot(abs(fft(s3)),'b')