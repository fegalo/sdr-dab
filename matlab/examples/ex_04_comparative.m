close all
clear
clc

T=10;
%%ifft comparative
x=[0 1 0];
y=ifft(x,T);
figure(1);
plot(real(y));
N=10;

xr=[x zeros(1,T-length(x))];
y2=ifft(xr);
figure(2);
plot(real(y2(:)));

%pulse and rcos comparative
xk1=[0 1 0 1 1 0 1];
fd=1;
xup1_pulse=upfirdn(xk1,ones(1,T),T,1);
delay_symbols=2;
pulse = rcosine(1,N,'normal',0.9,delay_symbols);
xup1_cos=upfirdn(xk1,pulse,N,1);
%xup1_cos=rcosflt(xk1,1,N,'fir',0.9);

figure(3);
subplot(211);%pulse downsampling
plot(real(xup1_pulse));hold on;grid on;
index=1:T:length(xup1_pulse-1);
xdown_pulse=xup1_pulse(index);
plot(index,real(xdown_pulse),'*','MarkerSize',8);
xlim([0 N*T]);

subplot(212);%rcos downsampling
plot(real(xup1_cos));hold on;grid on;
delay=delay_symbols*T;%default delay_symbols=3 in rcosine
index=(1:T:(length(xup1_cos)-2*delay))+delay;
xdown_cos=xup1_cos(index);
plot(index,real(xdown_cos),'*','MarkerSize',8);
xlim([delay N*T+delay]);