%Example of time-domain raised cosine

close all
clear
clc

%% Intento de coseno alzado del libro
% B=0.5;
% Tg=2;
% Tsub=5
% Tsym=Tg+Tsub;
% 
% n=1;
% 
% for t=-5:0.1:10
%     if     (-(Tg+B*Tsym/2)<=t) && (t<-(Tg-B*Tsym/2))
%         x(n)=0.5+0.5*cos(pi*(t+B*Tsym+Tg)/(B*Tsym));
%     elseif (-(Tg+B*Tsym/2)<=t) && (t<(Tsub-B*Tsym/2))
%         x(n)=1;
%     elseif ((Tsub-B*Tsym/2)<=t) && (t<=(Tsub+B*Tsym/2))
%         x(n)=0.5+0.5*cos(pi*(t-Tsub+B*Tsym)/(B*Tsym));
%     else
%        x(n)=0; 
%     end
%     n=n+1;
% end
% 
% t=-5:0.1:10;
% plot(t,x)

%% Coseno alzado sencillo
% B=0.35
% T=2;
% 
% 
% n=1;
% for t=-5:0.01:5
%     if     (abs(t)<((1-B)*T))
%         x(n)=1;
%     elseif (abs(t)>=((1-B)*T)) && (abs(t)<((1+B)*T))
%         x(n)=0.5+0.5*cos((pi/(2*T*B))*(abs(t)-(1-B)*T));
%     elseif (abs(t)<=((1+B)*T))
%         x(n)=0;
%     else
%        x(n)=0; 
%     end
%     n=n+1;
% end
% 
% t=-5:0.01:5;
% plot(t,x);
% figure
% psd(x)


% %% Coseno alzado para OFDM
% Tg=2;
% Tsub=6;
% Tsym=Tg+Tsub;
% alpha=0.5;
% 
% T=Tsym/2;
% B=alpha*Tg/Tsym;
% 
% n=1;
% for t=-10:0.01:10
%     if     (abs(t)<((1-B)*T))
%         x(n)=1;
%     elseif (abs(t)>=((1-B)*T)) && (abs(t)<((1+B)*T))
%         x(n)=0.5+0.5*cos((pi/(2*T*B))*(abs(t)-(1-B)*T));
%     elseif (abs(t)<=((1+B)*T))
%         x(n)=0;
%     else
%        x(n)=0; 
%     end
%     n=n+1;
% end
% 
% t=-10:0.01:10;
% plot(t,x);grid on

%% Coseno alzado para ofdm con fs

fs=10;         %100Hz
Tg=2;            %2s
Tsub=6;          %6s
Tsym=Tg+Tsub;    %8s
alpha=0.5;
% Si Tg=2s y Tsub=6s, Tsym= 8 segundos
% Con alpha = 0.5, se expande alpha*Tg=0.5*2=1s
% El formado va desde -4.5 a 4.5,(Tsym+alpha*Tg)

T=Tsym/2;
B=alpha*Tg/Tsym;
tiempo=-Tsym:(1/fs):Tsym;
n=1;
for t=tiempo
    if     (abs(t)<((1-B)*T))
        x(n)=1;
    elseif (abs(t)>=((1-B)*T)) && (abs(t)<((1+B)*T))
        x(n)=0.5+0.5*cos((pi/(2*T*B))*(abs(t)-(1-B)*T));
    elseif (abs(t)<=((1+B)*T))
        x(n)=0;
    else
       x(n)=0; 
    end
    n=n+1;
end

plot(tiempo,x);grid on