%QPSK bandabase

close all;
clear all;
clc;

%qpsk=[1 -1 i -i];
qpsk=[1+i 1-i i+1 -i-1];

fs=100;   %muestras/segundo - frecuencia de miestreo
R=5;      %simbolos/segundo
T=fs/R;   %muestras/simbolo - ¿periodo simbolo?
N=10;      %Numero de simbolos a transmitir en cada subportadora

xk1=qpsk(randi(length(qpsk),N,1));
xk2=qpsk(randi(length(qpsk),N,1));
xk3=qpsk(randi(length(qpsk),N,1));

t=[0:T];
s1=0;
for n=1:N
    s1=[s1 xk1(n)*exp(j*2*pi*1*t/T)];
end
s1=s1(2:end);
figure(1);
plot(imag(s1))


s2=0;
for n=1:N
    s2=[s2 xk2(n)*exp(j*2*pi*2*t/T)];
end
s2=s2(2:end);
figure(2);
plot(imag(s2))

s3=0;
for n=1:N
    s3=[s3 xk3(n)*exp(j*2*pi*3*t/T)];
end
s3=s3(2:end);
figure(3);
plot(imag(s3));

% Xk=[xk1;xk2;xk3].';
% y=ifft(Xk(1,:),T);
Xk=[xk1;xk2;xk3];
y=ifft(Xk,T);
y=y(:);
%Aplicar filtro cos y añadir Tguarda
%
figure(4)
subplot(311);
plot(abs(y));xlim([0 N*T])
subplot(312);
plot(abs(s1+s2+s3));xlim([0 N*T]);

