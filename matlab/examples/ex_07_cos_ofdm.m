%QPSK bandabase

close all;
clear all;
clc;

qpsk=[1 -1 i -i];
%qpsk=[1+i 1-i i+1 -i-1];

fs=1000;   %muestras/segundo - frecuencia de miestreo
R=5;      %simbolos/segundo
T=fs/R;   %muestras/simbolo - ¿periodo simbolo?
N=3;      %Numero de simbolos a transmitir en cada subportadora
Tg=fix(T/2);

xk1=qpsk(randi(length(qpsk),N,1));
xk2=qpsk(randi(length(qpsk),N,1));
xk3=qpsk(randi(length(qpsk),N,1));

t=[0:T];

% Xk=[xk1;xk2;xk3].';
% y=ifft(Xk(1,:),T);
Xk=[xk1;xk2;xk3];
y=T*ifft(Xk,T);
% Preparar ventana rcos
Tg_s=Tg/fs;          %2s
Tsub_s=T/fs;        %6s
alpha=0.9;
win=rcos_winofdm(fs,Tg_s,Tsub_s,alpha);
ext=(length(win)-(T+Tg))/2;
%
% Añadir tiempo de guarda a cada simbolo, includo el suavizado RC
y_zeros=[zeros(Tg+ext,N);y;zeros(ext,N)];
% Copiar cola en la cabecera de cada simbolo
y_head=y_zeros;
y_head([1:Tg+ext],:)=y([end-(Tg+ext)+1:end],:);
y_head((end-ext+1):end,:)=y(1:ext,:);
% Multiplicar por hrc (winofdm)
yhead_w=y_head.*(win'*ones(1,3));
% Falta hacer overlap
%
figure(1)
subplot(321);
plot(real(y_zeros(:)));xlim([0 N*(T+Tg)]);
subplot(322);
plot(imag(y_zeros(:)));xlim([0 N*(T+Tg)]);
subplot(323);
plot(real(y_head(:)));xlim([0 N*(T+Tg)]);
subplot(324);
plot(imag(y_head(:)));xlim([0 N*(T+Tg)]);
subplot(325);
plot(real(yhead_w(:)));xlim([0 N*(T+Tg)]);
subplot(326);
plot(imag(yhead_w(:)));xlim([0 N*(T+Tg)]);
