function [ winofdm ] = rcos_winofdm( fs,Tg, Tsub, alpha )
%RCOS_WINOFDM Summary of this function goes here
%   Detailed explanation goes here

Tsym=Tg+Tsub;

T=Tsym/2;
B=alpha*Tg/Tsym;
tiempo=-((Tsym+alpha*Tg)/2):(1/fs):((Tsym+alpha*Tg)/2);
n=1;

for t=tiempo
    if     (abs(t)<((1-B)*T))
        x(n)=1;
    elseif (abs(t)>=((1-B)*T)) && (abs(t)<((1+B)*T))
        x(n)=0.5+0.5*cos((pi/(2*T*B))*(abs(t)-(1-B)*T));
    elseif (abs(t)<=((1+B)*T))
        x(n)=0;
    else
       x(n)=0; 
    end
    n=n+1;
end
%plot(tiempo,x);grid on
% Remove 0 value to avoid odd length
winofdm=x([1:fix(end/2) round(end/2+1):end]);
end

