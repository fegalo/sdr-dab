close all
clear all
clc

fs=10;         %100Hz
Tg=2;          %2s
Tsub=6;        %6s
alpha=0.5;
win=rcos_winofdm(fs,Tg,Tsub,alpha);
plot(win)