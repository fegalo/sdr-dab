% Example of OFDM TX with some DAB parameters

close all;
clear all;
clc;

%qpsk=[1 -1 i -i];
qpsk=[1+i 1-i i-1 -i-1];

fs=2000000;          % samples/second
Nsub=fix(1000e-6*fs);% samples/simbol
Ng=fix(Nsub/4);      % samples of guard
Nsym=Nsub+Ng;        % samples/complete_symbol
L=10;                % Number_of_tx_symbols/carrier, length
carriers=500;

Xk=[];
pilot=qpsk(randi(length(qpsk),carriers,1));
for c=1:carriers
    Xk=[Xk;pilot(c) qpsk(randi(length(qpsk),L-1,1))];
end

yl=Nsub*ifft(Xk,Nsub);

% Prepare rcos_win
Tg_s=Ng/fs;
Tsub_s=Nsub/fs;
alpha=0.2;
win_rcos=rcos_winofdm(fs,Tg_s,Tsub_s,alpha);
win_ext=(length(win_rcos)-(Nsub+Ng))/2;
win_size=length(win_rcos);

% Add guard time
yl_zeros_noext=[zeros(Ng,L);yl];
% Add overlapping part of the window
yl_zeros=[zeros(Ng+win_ext,L);yl;zeros(win_ext,L)];
% Copy head in every symbol head
yl_head=yl_zeros;
yl_head([1:Ng+win_ext],:)=yl([end-(Ng+win_ext)+1:end],:);
yl_head((end-win_ext+1):end,:)=yl(1:win_ext,:);
% Multiply by win_rcos
yl_head_w=yl_head.*(win_rcos'*ones(1,L));
% Overlap symbols
y=zeros(L*Nsym+2*win_ext,1);
y(1:win_size)=yl_head_w(:,1);
for n=2:L
    y(((n-1)*win_size-2*(n-1)*win_ext+1):(n*win_size-2*(n-1)*win_ext))=y(((n-1)*win_size-2*(n-1)*win_ext+1):(n*win_size-2*(n-1)*win_ext))+yl_head_w(:,n);
end

save('ofdm_samples.mat','y','Xk','fs','L','Nsym','Nsub','Ng','win_ext');

%% Graficas
figure(1)
subplot(221);
plot(real(yl_zeros_noext(:)));xlim([0 L*(Nsym)+win_ext]);
ylabel('Re(y_l)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([n*Nsym n*Nsym],y1ine,'r');
    plot([n*Nsym+Ng n*Nsym+Ng],y1ine,'g');
end
subplot(222);
plot(imag(yl_zeros_noext(:)));xlim([0 L*(Nsym)+win_ext]);
ylabel('Imag(y_l)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([n*Nsym n*Nsym],y1ine,'r');
    plot([n*Nsym+Ng n*Nsym+Ng],y1ine,'g');
end

% y
subplot(223);
plot(real(y(:)));xlim([0 L*(Nsym)+2*win_ext]);
ylabel('Re(y)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([win_ext+n*Nsym win_ext+n*Nsym],y1ine,'r');
    plot([win_ext+n*Nsym+Ng win_ext+n*Nsym+Ng],y1ine,'g');
end
subplot(224);
plot(imag(y(:)));xlim([0 L*(Nsym)+2*win_ext]);
ylabel('Re(y)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([win_ext+n*Nsym win_ext+n*Nsym],y1ine,'r');
    plot([win_ext+n*Nsym+Ng win_ext+n*Nsym+Ng],y1ine,'g');
end

figure(2)
subplot(211);
psd(yl_zeros_noext(:));
subplot(212);
psd(y);