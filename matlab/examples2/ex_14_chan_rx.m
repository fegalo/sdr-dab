% Example of OFDM RX with some DAB parameters and channel estimation

clc
clear
close all

load('ofdm_samples.mat');

%Noise
SNRdB=10;
y = awgn(y,SNRdB,'measured');
h_ch=[complex(randn,randn),0,0,0,complex(randn,randn)/sqrt(16),complex(randn,randn)/sqrt(18)]/sqrt(2);
y=conv(y,h_ch);
figure(1)
subplot(211);
stem(real(h_ch));
ylabel('Re(h)')
subplot(212);
stem(imag(h_ch));
ylabel('Imag(h)')

%% Plot RX signal
figure(2)
pilot=Xk(:,1);
y1ine=get(gca,'ylim');
subplot(311);
plot(real(y(:)));xlim([0 L*(Nsym)+2*win_ext]);
ylabel('Re(y)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([win_ext+n*Nsym win_ext+n*Nsym],y1ine,'r');
    plot([win_ext+n*Nsym+Ng win_ext+n*Nsym+Ng],y1ine,'g');
end
subplot(312);
plot(imag(y(:)));xlim([0 L*(Nsym)+2*win_ext]);
ylabel('Re(y)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([win_ext+n*Nsym win_ext+n*Nsym],y1ine,'r');
    plot([win_ext+n*Nsym+Ng win_ext+n*Nsym+Ng],y1ine,'g');
end

subplot(313);
delay=200;
% Search the symbol start
[diff_value,fix_delay]=fix_STO(y.',Nsub,Ng,delay);
plot(abs(diff_value));
ylabel('Diff. Value')
xlim([0 L*(Nsym)+2*win_ext]);
delay_new=delay+fix_delay;
subplot(311)
plot([delay delay],y1ine,'k');
plot([delay_new delay_new],y1ine,'k');

%Manual delay
%delay_new=win_ext+1+8;

%% REcovery simbolos
x_rec=fft(y(delay_new+Ng:delay_new+Ng+Nsub-1))/Nsub;
x_rec=x_rec(1:500);
h_est=pilot./x_rec;%Chanel estimation,it fixes some phase errors
x_fil=h_est.*x_rec;
figure
subplot(211)
plot(x_rec,'.');xlim([-2 2]);ylim([-2 2])
title('First symbol set')
subplot(212)
plot(x_fil,'.');xlim([-2 2]);ylim([-2 2])

delay_new=delay_new+2*Nsym;
x_rec=fft(y(delay_new+Ng:delay_new+Ng+Nsub-1))/Nsub;
x_rec=x_rec(1:500);
x_fil=h_est.*x_rec;
figure
subplot(211)
plot(x_rec,'.');xlim([-2 2]);ylim([-2 2])
title('Second symbol set')
subplot(212)
plot(x_fil,'.');xlim([-2 2]);ylim([-2 2])

delay_new=delay_new+3*Nsym;
x_rec=fft(y(delay_new+Ng:delay_new+Ng+Nsub-1))/Nsub;
x_rec=x_rec(1:500);
x_fil=h_est.*x_rec;
figure
subplot(211)
plot(x_rec,'.');xlim([-2 2]);ylim([-2 2])
title('Third symbol set')
subplot(212)
plot(x_fil,'.');xlim([-2 2]);ylim([-2 2])

