% CAZAQ generator
% https://en.wikipedia.org/wiki/Zadoff%E2%80%93Chu_sequence
% http://www.etsi.org/deliver/etsi_en/300400_300499/300401/01.04.01_60/en_300401v010401p.pdf
% 14.13.2 Pg 147

clear all
close all
clc

zk=get_DABpilot(1);
plot(zk,'.')
unique(angle(zk)*360/(2*pi))