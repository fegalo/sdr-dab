function [diff_value fix_delay]=fix_STO(y,Nsub,Ng,delay)
fix_delay=0;
N_ofdm=Nsub+Ng; minimum=100; STO_est=0;
if nargin<4, delay = N_ofdm/2; end
for n=1:N_ofdm
   nn = n+delay+[0:Ng-1]; 
   tmp0 = abs(y(nn))-abs(y(nn+Nsub));  diff_value(n) = tmp0*tmp0'; % Eq.(5.12) is strong against CFO
end
[a fix_delay]=min(diff_value);