% DAB transmission signal
close all;
clear all;
clc

cw_size=2304;   % codeword size in bits
r_total=4;
L=3;            % OFDM symbols=3
K=1536;         % num of used ofdm carriers

i=[0:2303];
b=randint(r_total,cw_size);

%% 14.4.1 Block Partitioning
bp=zeros(1,r_total*cw_size);
for r=0:(r_total-1)
    ip=i+2304*mod(r,4);
    bp(ip+1)=b(r+1,i+1);
end
%pl=zeros(1,cw_size*4/3);
limite1=2*K;%3072
ip=[0:6*K-1];%9215
l=fix(ip/limite1)+2;
n=rem(ip,limite1);

p_ln=zeros(3,limite1);
p_ln(1,:)=bp(1:limite1);
p_ln(2,:)=bp(limite1+1:2*limite1);
p_ln(3,:)=bp(2*limite1+1:3*limite1);
% --- end 14.4.1 Block Partitioning

%% 14.5 QPSK symbol mapper
q_ln=zeros(L,K);
for l=1:3
    for n=0:(K-1)
        q_ln(l,n+1)=(1-2*p_ln(l,n+1))+j*(1-2*p_ln(l,n+1+K));
    end
end
% --- end 14.5 QPSK symbol mapper

%%  14.6 Frequency interleaving
prod=zeros(1,2047);
for i=1:2047
   prod(i+1)=mod(13*prod(i)+511,2048);
end
D = prod(find(prod > 255 & prod < 1793 & prod ~= 1024));
k=D-1024;
%y_lk = q_ln
y_lk=zeros(L,K);
% Convert (− K/2 ≤ k < 0) and (0 < k ≤ K/2) => (k~=0)
% to  1<k_index<K
k_neg=find(k<0);
k_index=k;
k_index(k_neg)=k_index(k_neg)+1;
k_index=k_index+K/2;
for l=1:L
    y_lk(l,:)=q_ln(l,k_index);
end
%TODO y_lk(k=0)=0 ?
% --- end 14.6 Frequency interleaving

%% RX
q_ln_rec=zeros(L,K);
p_ln_rec=zeros(L,2*K);
for l=1:L
    q_ln_rec(l,k_index)=y_lk(l,:);
    %isequal(q_ln_rec(l,:),q_ln(l,:))
    p_ln_rec(l,1:K)=(1-real(q_ln_rec(l,:)))/2;
    p_ln_rec(l,(K+1):2*K)=(1-imag(q_ln_rec(l,:)))/2;
    %isequal(p_ln_rec(l,:),p_ln(l,:))
end
%plot(abs(p_ln_rec(1,:)-p_ln(1,:)))
bp_rec=[p_ln_rec(1,:) p_ln_rec(2,:) p_ln_rec(3,:)];
%isequal(bp_rec,bp)
b_rec=zeros(r_total,cw_size);
i=[0:2303];
for r=0:(r_total-1)
    ip=i+2304*mod(r,4);
    b_rec(r+1,i+1)=bp_rec(ip+1);
end
isequal(b_rec,b)