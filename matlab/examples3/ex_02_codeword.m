% DAB transmission signal
close all;
clear all;
clc

I=768;
a_data=randint(1,I);

pi16=[1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0];
pi15=[1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 0 0];
vt = [1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0];

% % demo
% trellis =  poly2trellis([5 4],[23 35 0; 0 5 13]);
% data = randi([0 1],70,1);
% codedData = convenc(data,trellis);
% decodedData = vitdec(codedData,trellis,34,'trunc','hard');
% 
% biterr(data,decodedData)

% %
% try1
% trellis =  poly2trellis([7],[133 171 145 133]);
% x_cw = convenc(a_data,trellis);
% b=[];
% for i=1:21
%     x_cw1=x_cw((128*(i-1)+1):(128*(i)));
%     b=[b x_cw1(find([pi16 pi16 pi16 pi16]))];
% end
% for i=22:24
%     x_cw1=x_cw((128*(i-1)+1):(128*(i)));
%     b=[b x_cw1(find([pi15 pi15 pi15 pi15]))];
% end
% b=[b vt(find(vt==1))];%FIXME add 24 bits tail
% %length(b)=2304

% try 2
% trellis =  poly2trellis([7],[133 171 145 133]);
% a_data=[a_data zeros(1,6)];% final state 11.1.1 Mother code
% [x_cw f_state] = convenc(a_data,trellis);
% b=[];
% for i=1:21
%     x_cw1=x_cw((128*(i-1)+1):(128*(i)));
%     b=[b x_cw1(find([pi16 pi16 pi16 pi16]))];
% end
% for i=22:24
%     x_cw1=x_cw((128*(i-1)+1):(128*(i)));
%     b=[b x_cw1(find([pi15 pi15 pi15 pi15]))];
% end
% tail=x_cw((128*(25-1)+1):end);
% b=[b tail(find(vt))];%length(b)=2304


trellis =  poly2trellis([7],[133 171 145 133]);
a_data=[a_data zeros(1,6)];% final state 11.1.1 Mother code

puncpat=[];
for i=1:21
    puncpat=[puncpat pi16 pi16 pi16 pi16];
end
for i=22:24
    puncpat=[puncpat pi15 pi15 pi15 pi15];
end
puncpat=[puncpat vt];
b = convenc(a_data,trellis,puncpat);

a_decod = vitdec(b,trellis,34,'trunc','hard',puncpat);
biterr(a_data,a_decod)

