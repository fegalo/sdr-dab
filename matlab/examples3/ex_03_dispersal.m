% DAB transmission signal
close all;
clear all;
clc;

I=768;
data=randint(1,I);

%TODO implement PN generator
h = commsrc.pn('GenPoly',      [1 0 0 0 0 1 0 0 0 1], ...
              'InitialStates', [1 1 1 1 1 1 1 1 1],   ...
              'NumBitsOut',    1);
set(h, 'NumBitsOut', I+9);
pn=generate(h);
pn=pn(10:end).';
%pn(1:16)

scram=xor(data,pn);
data_rec=xor(scram,pn);
biterr(data,data_rec)

%5.2.1 Fast Information Block (FIB)
x=randint(1,240);%240+16CRC=256

crc_gen = comm.CRCGenerator('Polynomial',[16 15 2 0]);
y = step(crc_gen,x.').';
%y(7:27)=1;%introduce some errors
crc_det = comm.CRCDetector('Polynomial',[16 15 2 0]);
[x_rec err]= step(crc_det,y.');
err
