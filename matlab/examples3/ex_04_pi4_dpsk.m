% PI/4 DPSK
close all;
clear all;
clc;

qpsk=[1+i 1-i -1+i -1-i]/sqrt(2);
L=5;
K=1536;
y=qpsk(randi(length(qpsk),L,K));
%plot(y(1,:),'.')

z=zeros(L,K);
z(1,:)=y(1,:);
for i=2:L
    z(i,:)=z(i-1,:).*y(i,:);
end

subplot(511)
plot(z(1,:),'.')
subplot(512)
plot(z(2,:),'.')
subplot(513)
plot(z(3,:),'.')
subplot(514)
plot(z(4,:),'.')
subplot(515)
plot(z(5,:),'.')

r=zeros(L,K);
r(1,:)=z(1,:);
for i=2:L
    r(i,:)=z(i,:)./z(i-1,:);
end

isequal(y,r)