function [ zk ] = get_DABpilot( mode )
%GET_DABPILOT Summary of this function goes here
%   Detailed explanation goes here
H_table= [0 2 0 0 0 0 1 1 2 0 0 0 2 2 1 1 0 2 0 0 0 0 1 1 2 0 0 0 2 2 1 1; ...
    0 3 2 3 0 1 3 0 2 1 2 3 2 3 3 0 0 3 2 3 0 1 3 0 2 1 2 3 2 3 3 0; ...
    0 0 0 2 0 2 1 3 2 2 0 2 2 0 1 3 0 0 0 2 0 2 1 3 2 2 0 2 2 0 1 3; ...
    0 1 2 1 0 3 3 2 2 3 2 1 2 1 3 2 0 1 2 1 0 3 3 2 2 3 2 1 2 1 3 2];
K=768
for k=-K:K
    [ kp i n ] = get_params(k);
    hik=H_table(i+1,k-kp+1);
    phi_k=pi/2*(hik+n);
    zk(k+K+1)=exp(j*phi_k);
end
zk(K+1)=0;
zk=zk.';
end

