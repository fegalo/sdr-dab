function [ xk ] = qpsk_demod( xk, type_mod)
if nargin < 2
    type_mod=1;
end

if type_mod==1
    xk(find(real(xk)>0 & imag(xk)>0))=(1+i)/sqrt(2);
    xk(find(real(xk)>0 & imag(xk)<0))=(1-i)/sqrt(2);
    xk(find(real(xk)<0 & imag(xk)>0))=(-1+i)/sqrt(2);
    xk(find(real(xk)<0 & imag(xk)<0))=(-1-i)/sqrt(2);
%     values = [1+i -1+i i-1 -i-1];
%     xk = interp1(values,values,xk,'nearest')
else
    k1=find(angle(xk)>+1*pi/4 & angle(xk)<=+3*pi/4);
    k2=find(angle(xk)>+3*pi/4 | angle(xk)<=-3*pi/4);
    k3=find(angle(xk)>-3*pi/4 & angle(xk)<=-1*pi/4);
    k4=find(angle(xk)>-1*pi/4 & angle(xk)<=+1*pi/4);
    xk(k1)=i;
    xk(k2)=-1;
    xk(k3)=-i;
    xk(k4)=1;

%     values = [1 -1 i -i];
%     xk = interp1(xk,xk,values,'nearest')
end

