% Example of OFDM RX with some DAB parameters and channel estimation in
% each symbol

clc
clear
close all

load('sdr_samples.mat');
fs=2048000;
Nsub=2048;
Ng=504;
Nsym=Ng+Nsub;
Nnull=2656;
K=1536;
K0=246;
%L=76;
L=12;
pilot=get_DABpilot(1);

inicio=(77+52)*Nsym;
s=s(inicio+1:inicio+L*Nsym);

%% Plot RX signal
figure(1)
y1ine=get(gca,'ylim');
subplot(313);
delay=3000;
% Search the symbol start
[diff_value,fix_delay]=fix_STO(s.',Nsub,Ng,delay);
plot(abs(diff_value));
ylabel('Diff. Value')
xlim([0 L*(Nsym)]);
delay_new=delay+fix_delay;
subplot(311)
plot(real(s(:)));xlim([0 L*(Nsym)]);
ylabel('Re(y)')
y1ine=get(gca,'ylim');
hold on
for n=0:L
    plot([delay_new+n*Nsym delay_new+n*Nsym],y1ine,'r');
    plot([delay_new+n*Nsym+Ng delay_new+n*Nsym+Ng],y1ine,'g');
end
subplot(312);
plot(imag(s(:)));xlim([0 L*(Nsym)+2*delay_new]);
ylabel('Im(y)')
y1ine=get(gca,'ylim');xlim([0 L*(Nsym)]);
hold on
for n=0:L
    plot([delay_new+n*Nsym delay_new+n*Nsym],y1ine,'r');
    plot([delay_new+n*Nsym+Ng delay_new+n*Nsym+Ng],y1ine,'g');
end
plot([delay delay],y1ine,'k');
plot([delay_new delay_new],y1ine,'k');


%Manual delay
%delay_new=win_ext+1+8;
z_lk=zeros(5,K+1);
%% Recovery simbolos
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1))/Nsub);
x_rec=x_rec(K0:K0+K);
h_est=pilot./x_rec;%Chanel estimation,it fixes some phase errors
z_lk(1,:)=pilot;
x_fil=h_est.*x_rec;
figure
subplot(211)
plot(x_rec,'.');
title('1st symbol set(pilot)')
subplot(212)
plot(x_fil(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,1);
z_lk(2,:)=x;
h_est=x./x_rec;%Chanel estimation
figure
subplot(311)
plot(x_rec,'.');
title('2nd symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,2);
z_lk(3,:)=x;
h_est=x./x_rec;%Chanel estimation
figure
subplot(311)
plot(x_rec,'.');
title('3rd symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,1);
h_est=x./x_fil;%Chanel estimation
z_lk(4,:)=x;
figure
subplot(311)
plot(x_rec,'.');
title('4th symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,2);
h_est=x./x_rec;%Chanel estimation
figure
subplot(311)
plot(x_rec,'.');
title('5th symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,1);
h_est=x./x_rec;%Chanel estimation
figure
subplot(311)
plot(x_rec,'.');
title('6th symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,2);
h_est=x./x_rec;%Chanel estimation
figure
subplot(311)
plot(x_rec,'.');
title('7th symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

delay_new=delay_new+Nsym;
x_rec=fftshift(fft(s(delay_new+Ng:delay_new+Ng+Nsub-1)))/Nsub;
x_rec=x_rec(K0:K0+K);
x_fil=h_est.*x_rec;
x=qpsk_demod(x_fil,1);
h_est=x./x_rec;%Chanel estimation
figure
subplot(311)
plot(x_rec,'.');
title('8th symbol set')
subplot(312)
plot(x_fil,'.');
subplot(313)
plot(x(1:end ~= 769),'r*');

save('z_lk_symbols.mat','z_lk');