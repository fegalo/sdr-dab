% Recover bits from symbols
% NOT WORKING

clc
clear
close all

load('z_lk_symbols');
z_lk=z_lk(:,([1:768 770:end]));


L=3;
K=1536;
r_total=4;
cw_size=2304;   % codeword size in bits

%% pi-DPSK/
y_lk=zeros(L,K);
y_lk(1,:)=z_lk(1,:);
for i=2:L+1
    y_lk(i,:)=z_lk(i,:)./z_lk(i-1,:);
end
y_lk=y_lk(2:end,:)*(sqrt(2));%remove pilot

%% pre-Block
prod=zeros(1,2047);
for i=1:2047
   prod(i+1)=mod(13*prod(i)+511,2048);
end
D = prod(find(prod > 255 & prod < 1793 & prod ~= 1024));
k=D-1024;
% Convert (− K/2 ≤ k < 0) and (0 < k ≤ K/2) => (k~=0)
% to  1<k_index<K
k_neg=find(k<0);
k_index=k;
k_index(k_neg)=k_index(k_neg)+1;
k_index=k_index+K/2;
%%block
q_ln_rec=zeros(L,K);
p_ln_rec=zeros(L,2*K);
for l=1:L
    q_ln_rec(l,k_index)=y_lk(l,:);
    p_ln_rec(l,1:K)=(1-real(q_ln_rec(l,:)))/2;
    p_ln_rec(l,(K+1):2*K)=(1-imag(q_ln_rec(l,:)))/2;
end
bp_rec=[p_ln_rec(1,:) p_ln_rec(2,:) p_ln_rec(3,:)];
b_rec=zeros(r_total,cw_size);
i=[0:2303];
for r=0:(r_total-1)
    ip=i+2304*mod(r,4);
    b_rec(r+1,i+1)=bp_rec(ip+1);
end

%% CONVOLUTIONAL CODE b_rec
b=round([b_rec(1,:)]);
pi16=[1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0];
pi15=[1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 1 0 1 1 0 0];
vt = [1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0 1 1 0 0];
trellis =  poly2trellis([7],[133 171 145 133]);
puncpat=[];
for i=1:21
    puncpat=[puncpat pi16 pi16 pi16 pi16];
end
for i=22:24
    puncpat=[puncpat pi15 pi15 pi15 pi15];
end
puncpat=[puncpat vt];
I=768;
a_decod = vitdec(b,trellis,34,'trunc','hard',puncpat);
a_decod=a_decod(1:(end-6));%TODO Check
%% remove PN 
%TODO implement PN generator
h = commsrc.pn('GenPoly',      [1 0 0 0 0 1 0 0 0 1], ...
              'InitialStates', [1 1 1 1 1 1 1 1 1],   ...
              'NumBitsOut',    1);
set(h, 'NumBitsOut', I+9);
pn=generate(h);
pn=pn(10:end).';
data_rec=xor(a_decod,pn);

%% FIB
crc_det = comm.CRCDetector('Polynomial',[16 15 2 0]);
[x_rec err]= step(crc_det,data_rec.');
err

