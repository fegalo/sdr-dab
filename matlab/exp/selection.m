% Select samples from sdr file
clc
clear all
close all;

load('file_sdr.mat')

fs=2048000;
Nsub=2048;
Ng=504;
Nsym=Ng+Nsub;
Nnull=2656;
K=1536;
L=76;

t1=5;
t2=8;
s=data(t1*fs:t2*fs,1)+i*data(t1*fs:t2*fs,2);
%plot(s(:,1));
clear data
save('sdr_samples.mat','s','fs');
